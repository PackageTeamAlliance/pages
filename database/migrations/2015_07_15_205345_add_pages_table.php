<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('uri');
            $table->string('type');
            $table->string('layout')->nullable();
            $table->string('section')->nullable();            
            $table->text('content')->nullable();
            $table->string('file')->nullable();
            $table->boolean('active');

            $table->timestamps();
            
            $table->engine = 'InnoDB';
            $table->unique('uri');
            $table->index(['uri', 'active']);
        });


        Schema::create('page_translations', function ($table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')->references('id')->on('pages')->after('id');
            $table->string('uri');
            $table->string('page_locale');
            $table->string('locale')->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->unique('page_locale');
            $table->index(['locale', 'uri']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
        Schema::drop('page_translations');
    }
}
