<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages_ab_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('page_id')->unsigned();
            $table->integer('visitors')->unsigned()->default(0);
            $table->integer('engagement')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages_ab_tests');
    }
}
