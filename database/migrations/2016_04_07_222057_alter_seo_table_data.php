<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeoTableData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::table('seo')->where([ 'entity_type' => 'Pta\Pages\Entities\Page'])->count()) {
            DB::table('seo')->update([ 'entity_type' => 'Pta\Pages\Models\Page']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('seo')->update([ 'entity_type' => 'Pta\Pages\Entities\Page']);
    }
}
 