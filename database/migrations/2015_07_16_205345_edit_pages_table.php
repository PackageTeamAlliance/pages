<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_translations', function ($table) {

            $table->string('layout')->after('content')->nullable();
            $table->string('section')->after('layout')->nullable();
            $table->string('type')->after('section')->nullable();
            $table->string('file')->after('type')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_translations', function($table){

            $table->dropCoumn('type');
            $table->dropCoumn('file');
            $table->dropCoumn('section');
            $table->dropCoumn('type');
        });
    }
}
