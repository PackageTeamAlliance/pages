<?php

namespace Modules\Pages\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Menus\src\Entities\Menu;
use Illuminate\Database\Eloquent\Model;
use Modules\Pages\Database\Seeders\PermissionSeeder;
use Modules\Pages\Database\Seeders\DefaultPageSeeder;

class PagesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(DefaultPageSeeder::class);
    }
}
