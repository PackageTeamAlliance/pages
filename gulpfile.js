var elixir = require('laravel-elixir');


elixir(function(mix) {

    mix
        .browserify([
            'components/page-script.js',

        ], './resources/assets/js/bundle/page-script.js')

        .browserify([
            'components/page-ab-script.js',

        ], './resources/assets/js/bundle/page-ab-script.js')
        .scripts([
            '../../../node_modules/vue/dist/vue.js',
            '../../../node_modules/vue-resource/dist/vue-resource.js',
            '../../../node_modules/selectize/dist/js/standalone/selectize.js',
            'slugify.js'
        ], './resources/assets/modules/pages/pages.js')

        .scripts([
            'bundle/page-script.js'

        ], './resources/assets/modules/pages/pages-index.js')

        .scripts([
            'bundle/page-ab-script.js'

        ], './resources/assets/modules/pages/pages-ab-index.js')

        .styles([
            '../../../node_modules/selectize/dist/css/selectize.bootstrap3.css',
        ], './resources/assets/modules/pages/pages.css')
});
