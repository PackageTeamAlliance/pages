import Vue from 'vue';
var moment = require('moment');
var VueTables = require('vue-tables');
var resource = require('vue-resource');
var editable = require('./mixins/editable.js');
var deleteable = require('./mixins/deleteable.js');

Vue.use(resource);
Vue.use(VueTables.server, {
  compileTemplates: true,
  highlightMatches: true,
  filterByColumn: false,
  texts: {
    filter: "Search : "
  },
  datepickerOptions: {
    showDropdowns: false
  }
  //skin:''
});

new Vue({
  el: "#pages-table",

  mixins: [editable, deleteable],

  ready : function(){
    // this.update_uri = '/admin/pages/update/';
  },

  methods : {

    deletePage : function(event)
    { 
      this.deleteEntry(event.target)
    }
  },
  computed: {

  },
  data: {
    columns:['id','name','type', 'active', 'uri'],
    options: {
      headings: {
        id: 'id',
        name: 'Name',
        type: 'Type',
        active: 'Active',
        uri: 'URL',
        view: 'Stats',
        edit: 'Edit'
      },
      templates: {
       type: function(row)
       {
        if(row.type === 'database')
        {
          return '<span class="label label-success">database</span>';
        }else{
          return '<span class="label label-default">file</span>';
        }
      },
      uri: function(row) {
          return '<span class="label label-default">'+ document.querySelector('meta[name="app_url"').getAttribute('content') + row.uri + '</span>';
      },
      active: function(row)
      {
        if(row.active)
        {
          return '<span class="label label-success">Active</span>';
        }else{
          return '<span class="label label-default">Deactive</span>';
        }
      },
      view: '<a href="/admin/pages/ab/view/{id}" class="btn btn-xs btn-warning">view</a>',
      edit: "<a href='/admin/pages/ab/edit/{id}' class='btn btn-xs btn-success'>edit</a>",
      delete: '<button v-on:click="$parent.deletePage" data-url="/admin/pages/ab/delete/{id}" data-entry_id="{id}"data-method="post" class="btn btn-xs btn-danger">Delete</button>'
    },
    customFilters: [],
  }
}
});


