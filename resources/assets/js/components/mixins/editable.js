module.exports = {
	data : function()
  {
    return {
        editMode : false,
        url : '',
        pk : -1,
        value: ''
    }
  },
  methods: {
    toggleStatus : function(object){
      const _self = this;
      this.uri = object.dataset.url;
      this.pk = object.dataset.pk;
      this.value = object.dataset.active;

      swal({
        title: "Warning",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancel",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, toggle status",
        closeOnConfirm: true
      }, function(confirmed) {
        if (confirmed)
        {
          const _token = document.querySelector('meta[name="_token"').getAttribute('content');

          _self.$http.post(_self.uri, {'_token' : _token, 'value' : _self.value, 'pk': _self.pk })
          .then(
            function(data){
              _self.$children[0].refresh();
            },
            function(error){

            }
          );
        }
      });
      
    }
  },

  computed: {
  }
};