import Vue from 'vue';
var moment = require('moment');
var VueTables = require('vue-tables');
var resource = require('vue-resource');
var editable = require('./mixins/editable.js');
var deleteable = require('./mixins/deleteable.js');

Vue.use(resource);
Vue.use(VueTables.server, {
  compileTemplates: true,
  highlightMatches: true,
  filterByColumn: false,
  texts: {
    filter: "Search : "
  },
  datepickerOptions: {
    showDropdowns: false
  }
});

new Vue({
  el: "#pages-table",

  mixins: [deleteable, editable],
  component : [],

  methods : {

    deletePage : function(event)
    { 
      this.deleteEntry(event.target);
    },

    togglePageStatus : function(event)
    {
      this.toggleStatus(event.target)
    }
  },
  computed: {
    // a computed getter
    b: function () {
      // `this` points to the vm instance
      return 1
    }
  },
  data: {
    columns:['id','name','type', 'active', 'uri'],
    row: {},
    options: {
      headings: {
        id: 'id',
        name: 'Name',
        type: 'Type',
        active: 'Status',
        uri: 'URL',
        actions: 'Actions',
        translations: 'Translations',
        delete: ''
      },
      templates: {
        type: '<span class="label label-default">{type}</span>',
        uri: function(row) {
          return '<span class="label label-default">'+ document.querySelector('meta[name="app_url"').getAttribute('content') + row.uri + '</span>';
        },
        type: function(row)
        {
          if(row.type === 'database')
          {
            return '<span class="label label-success">database</span>';
          }else{
            return '<span class="label label-default">file</span>';
          }
        },

        active: function(row)
        {
          var status ="Disable";
          if(! row.active)
          {
            status ="Activate";
          }
          
          return '<button v-on:click="$parent.togglePageStatus" data-url="pages/update/" data-pk="'+ row.id+'" class="btn btn-xs btn-info">'+ status +'</button>';
        },

        actions: function(row){  

          return "<a href='/admin/pages/"+ row.id+ "/copy' class='btn btn-xs btn-success'>copy</a> "+
          "<a href='/admin/pages/"+ row.id+ "/edit' class='btn btn-xs btn-success'>edit</a> "+
          "<a href='/admin/pages/"+ row.id+ "/translate' class='btn btn-xs btn-success'>translate</a> ";
        },
        translations: function(row){
          let $html = '';
          for (var i = row.translation.length - 1; i >= 0; i--) {
            $html += "<a href='/admin/pages/edit/"+ row.id + "/translate/" + row.translation[i].id + "' class='btn btn-xs btn-success'>" +row.translation[i].locale + "</a> ";
          };
          return $html;
        },
        delete: '<button v-on:click="$parent.deletePage" data-url="pages/{id}/delete/" data-entry_id="{id}"data-method="post" class="btn btn-xs btn-danger">Delete</button>'
      },
      customFilters: [],
    }
  }
});