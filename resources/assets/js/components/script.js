import Vue from 'vue';
var moment = require('moment');
var VueTables = require('vue-tables');
var resource = require('vue-resource');
var editable = require('./mixins/editable.js');
var deleteable = require('./mixins/deleteable.js');

Vue.use(resource);
Vue.use(VueTables.server, {
  compileTemplates: false,
  highlightMatches: true,
  filterByColumn: false,
  texts: {
    filter: "Search : "
  },
  datepickerOptions: {
    showDropdowns: false
  }
  //skin:''
});

new Vue({
  el: "#pages-table",

  mixins: [editable, deleteable],

  ready : function(){
    // this.update_uri = '/admin/pages/update/';
  },

  methods : {

    deletePage : function(event)
    { 
      this.deleteEntry(event.target)
    }
  },
  computed: {

  },
  data: {
    options: {
      columns:['id','name','type', 'active', 'uri'],
      headings: {
        id: 'id',
        name: 'Name',
        type: 'Type',
        active: 'Active',
        uri: 'URL',
        edit: '',
        copy: '',
        delete: ''
      },
      templates: {
        type: '<span class="label label-default">{type}</span>',
        uri: function(row) {
          return '<span class="label label-default">'+ document.querySelector('meta[name="app_url"').getAttribute('content') + row.uri + '</span>';
        },
        // uri: '<span class="label label-default">@{{b}}{uri}</span>',
        // active: "<button v-on:click='$parent.deleteEntry()' data-entry-id='{id}'><i class='glyphicon glyphicon-erase'></i>poop</button>",

        edit: "<a href='/admin/pages/{id}/edit' class='btn btn-xs btn-warning'>copy</a>",
        copy: "<a href='/admin/pages/{id}/copy' class='btn btn-xs btn-success'>edit</a>",
        delete: '<button v-on:click="$parent.deletePage" data-url="pages/{id}/delete/" data-entry_id="{id}"data-method="post" class="btn btn-xs btn-danger">Delete</button>'
      },
      customFilters: [],
    }
  }
});


