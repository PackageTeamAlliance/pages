<?php

return [

	'general' => [
		'id'          => 'Id',
		'name'        => 'Name',
		'uri'         => 'URL',
		'middleware'  => 'Middleware',
		'route'       => 'Route',
		'type'        => 'Type',
		'slug'        => 'Slug',
		'layout'      => 'Layout',
		'section'     => 'Section',
		'active'      => 'Active',
		'ab'          => 'AB Testing',
		'content'     => 'Content',
		'file'        => 'File',
		'locale'      => 'Language',
		'preview'     => 'preview',
		'translation' => 'Translations',
		'created_at'  => 'Created At',
		'name_help'   => 'Enter the Name here',
		'slug_help'   => 'Enter the Slug here',
		'uri_help'    => 'Enter the URL here',
		'type_help'   => 'Enter the Type here',
		'html_help'   => 'Enter the Html here',
		'content_help'=> 'Enter the content here',
		'file_help'   => 'Enter the File here',
		'locale_help' => 'Enter the Language translation',

	],

];
