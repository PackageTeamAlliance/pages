<?php

return [

	// General messages
	'not_found' => 'Page [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Successfully created.',
		'update' => 'Successfully updated.',
		'delete' => 'Successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the Page. Please try again.',
		'update' => 'There was an issue updating the Page. Please try again.',
		'delete' => 'There was an issue deleting the Page. Please try again.',
	],

];
