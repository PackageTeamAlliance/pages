<?php 

return [
		

	'title' => [
	
		'index' => 'All Pages',
		'meta' => 'Page Meta',
	
	], 

	'header' => [
	
		'index' => 'Page',
	
	], 

	'actions' => [
	
		'add'    => 'Add Page',
		'edit'   => 'Edit Page',
		'delete' => 'Delete Page',
	]

];