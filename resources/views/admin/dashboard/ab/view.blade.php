@extends ('backend.layouts.master')

@section('page-title')
@stop

@section('main-panel-title')
@stop

@section('after-styles-end')


@stop

@section('content')
<div class="box">
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-success">
					<!-- Default panel contents -->
					<div class="panel-heading">
						Page A View
						<button class="btn btn-default pull-right">Set as Page</button>
						<div class="clearfix"></div>
					</div>
					<!-- List group -->
					<ul class="list-group">
						<li class="list-group-item"><i class="fa fa-globe small"></i> <b>Views: </b> <span class="label label-success">{{$page->getAb->a_view }}</span></li>
						<li class="list-group-item"><i class="fa fa-globe small"></i> <b>Percent of Requests: </b> <span class="label label-success">@if($page->getAb->a_view) {{ round( ($page->getAb->a_view / $page->getAb->requests) * 100 , 2)}}% @else 0% @endif</span></li>
						<li class="list-group-item"><i class="fa fa-mouse-pointer small" aria-hidden="true"></i> <b>Total Clicks:</b> <span class="label label-primary">{{$page->getAb->a_clicks or 0}}</span></li>
						<li class="list-group-item"><i class="fa fa-mouse-pointer small" aria-hidden="true"></i> <b>Click rate:</b> <span class="label label-warning">@if($page->getAb->a_clicks) {{ round( ($page->getAb->a_clicks / $page->getAb->a_view) * 100, 2) }} @else 0 @endif %</span></li>
					</ul>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-warning">
					<!-- Default panel contents -->
					<div class="panel-heading">
						Page B View
						<button class="btn btn-default pull-right">Set as Page</button>
						<div class="clearfix"></div></div>

						<!-- List group -->
						<ul class="list-group">
							<li class="list-group-item"><i class="fa fa-globe small"></i> <b>Views: </b> <span class="label label-success">{{ $page->getAb->b_view or 0}}</span></li>
							<li class="list-group-item"><i class="fa fa-globe small"></i> <b>Percent of Requests: </b> <span class="label label-success">@if($page->getAb->b_view) {{ round( ($page->getAb->b_view / $page->getAb->requests) * 100 , 2)}}% @else 0% @endif</span></li>
							<li class="list-group-item"><i class="fa fa-mouse-pointer " aria-hidden="true"></i> <b>Total Clicks:</b> <span class="label label-primary">{{$page->getAb->b_clicks or 0}}</span></li>
							<li class="list-group-item"><i class="fa fa-mouse-pointer small" aria-hidden="true"></i> <b>Click rate:</b> <span class="label label-warning">@if($page->getAb->b_clicks) {{ round( ($page->getAb->b_clicks / $page->getAb->b_view) * 100, 2) }}% @else 0% @endif</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	@stop

	@section('scripts')

	@stop