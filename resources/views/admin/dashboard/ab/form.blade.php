@extends ('backend.layouts.master')

{{-- Page title --}}
@section('page-title')
@parent

@stop

@section('main-panel-title')
@parent

@stop

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/pages/pages.css')}}">
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
@stop


{{-- Page --}}
@section('content')
<div class="box box-success">
    <div class="box-body">
        <div class="box-body" id="pageForm">
            <h3 class="box-title">{{{ trans("action.{$mode}") }}} {{ trans('pta/pages::common.title') }}</h3>
            <div class="row nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#page">Page</a></li>
                </ul>
                {!! BootForm::openHorizontal(['sm' => [2, 9],'md' => [2, 9]]) !!}

                <div class="tab-content">
                    <div class="tab-pane active" id="page">

                        <input type="hidden" name="page_id" value="{{ $page->id ? : '' }}">
                        
                        <fieldset style="padding-top:40px;">
                            {!! BootForm::text('AB Test Name', 'name')->value(old('section', $page->getAb->name ? : ''))->attribute('id', 'name') !!}

                            <div class="form-group @if($errors->has('content') ) has-error @endif" v-show="database">
                                <label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.content') }}}</label>
                                <div class="col-xs-9">
                                    <textarea name="content" id="content" cols="30" rows="10">{{old('content', $page->content)}}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group @if($errors->has('type') ) has-error @endif">

                                <label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.type') }}}</label>

                                <div class="col-xs-9">
                                    <select class="form-control select" name="type" id="type" v-el:typeselect>
                                        <option @if(old('type', $page->type) === 'database') selected @endif value="database">Database</option> 
                                        <option @if(old('type', $page->type) === 'file') selected @endif value="file">File</option> 
                                    </select>
                                    <span class="help-block">{{{$errors->first('type')}}}</span>    
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('file') ) has-error @endif" v-show="!database">

                                <label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.file') }}}</label>

                                <div class="col-xs-9">
                                    <select class="form-control select" name="file" id="file" v-el:file>
                                        <option value="">---</option>
                                        @foreach($pages as $p)
                                        <option value="{{{ str_replace('.blade.php','', $p->getRelativePathName())}}}" @if( old('file', $page->getAbKey('file') ) === str_replace('.blade.php','', $p->getRelativePathName())  ) selected @endif >{{{ str_replace('.blade.php','', $p->getRelativePathName())}}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{{$errors->first('file')}}}</span>    
                                </div>

                            </div>

                            <div v-show="database">
                                {!! BootForm::text(trans('pta/pages::model.general.section'), 'section')->value(old('section', $page->getAbKey('section') ? : 'content'))->attribute('id', 'section') !!}
                            </div>

                            <div class="form-group  @if($errors->has('layout') ) has-error @endif">

                                <label class="col-xs-2 control-label" for="layout">{{{ trans('pta/pages::model.general.layout') }}}</label>

                                <div class="col-xs-9">
                                    <select class="form-control select" name="layout" id="layout" v-el:layout>
                                        <option value="">---</option>
                                        @foreach($layouts as $layout)
                                        <option value="{{{ config('pages.layout_path.name'). str_replace('.blade.php','',$layout->getRelativePathName()) }}}" 
                                            @if( old('layout', $page->getAbKey('layout')) === config('pages.layout_path.name'). str_replace('.blade.php','',$layout->getRelativePathName())  ) 
                                            selected 
                                            @endif
                                            > {{{ config('pages.layout_path.name'). str_replace('.blade.php','',$layout->getRelativePathName()) }}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <span class="help-block">{{{$errors->first('layout')}}}</span>  

                                </div>

                            </fieldset>
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="col-xs-2 control-label" for="submit">{{{ trans('action.save') }}}</label>

                        <div class="col-xs-9">
                            <button type="submit" class="btn btn-success" id="save"><i class="fa fa-save"></i> {{{ trans('action.save') }}}</button>
                        </div>

                    </div>
                    {!! BootForm::close() !!}

                </div>
            </div>
        </div>
    </div>
    @stop


    @section('scripts')
    <script src="{{url('module/assets/pages/pages.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
    <script>

        var pageForm = new Vue({
            el: '#pageForm',
            data: {
                database: @if( old('type', $page->type) === 'database' || old('type', $page->type) === null ) true
                @else false @endif ,
                type_select: null
            },
            ready: function () {
                $(this.$els.layout).selectize({
                    create: false,
                    sortField: 'text'
                });

                $(this.$els.file).selectize({
                    create: false,
                    sortField: 'text'
                });

                $(this.$els.active).selectize({
                    create: false,
                    sortField: 'text'
                });

                $(this.$els.ab).selectize({
                    create: true,
                    sortField: 'text'
                });

                var select = $(this.$els.typeselect).selectize({
                    create: false,
                    sortField: 'text'
                });
                
                this.type_select = select[0].selectize;
                this.type_select.on('change', this.toggleType);

            },
            methods: {

                toggleType: function (e) {
                    var value = this.type_select.getValue();
                    if (value === 'database') {

                        this.database = true;
                    } else {
                        this.database = false;
                    }

                }
            }
        });



$(function () {
    $('#content').summernote({
        height: 300,
        tabsize: 2,
        lang: '{{app()->getLocale()}}',
        'fontName':  'Source Sans Pro'
    });
});

$('.nav-tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})


</script>


@stop
