@extends ('backend.layouts.master')

{{-- Page title --}}
@section('page-title')
@parent
Create AB Test
@stop

@section('main-panel-title')
@parent
Create AB Test
@stop

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/pages/pages.css')}}">
@stop


{{-- Page --}}
@section('content')
<div class="box box-success">
	<div class="box-body">
		<div class="box-body" id="pageAbForm">
			<h3 class="box-title">Create AB Test</h3>

			{!! BootForm::openHorizontal(['sm' => [2, 9],'md' => [2, 9]]) !!}

			{!! BootForm::text('Test Name', 'name')->value(old('name'))->attribute('id', 'name') !!}


			<div class="form-group @if($errors->has('name') ) has-error @endif ">
				<label class="col-xs-2 control-label" for="page_id">Page</label>
				<div class="col-xs-9">
					<select class="form-control select" name="page_id" id="page_id" v-el:page_id>
						<option value="">---</option>
						@foreach($pages as $page)
						<option value="{{$page->id}}">
							{{$page->name}}
						</option>

						@endforeach
					</select>

				</div>

			</div>



			<div class="form-group">
				<label class="col-xs-2 control-label" for="submit">{{{ trans('action.save') }}}</label>

				<div class="col-xs-9">
					<button type="submit" class="btn btn-success" id="save"><i class="fa fa-save"></i> {{{ trans('action.save') }}}</button>
				</div>
			</div>
			{!! BootForm::close() !!}

		</div>
	</div>
</div>

@stop


@section('scripts')
<script src="{{url('module/assets/pages/pages.js')}}"></script>
<script>
	$('#page_id').selectize({
		create: false,
		sortField: 'text'
	});




</script>


@stop
