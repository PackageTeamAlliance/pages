@extends ('backend.layouts.master')

{{-- Page title --}}
@section('page-title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('pta/pages::common.title') }}
@stop

@section('main-panel-title')
@parent
{{{ trans("action.{$mode}") }}}
@stop

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/pages/pages.css')}}">
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
@stop


{{-- Page --}}
@section('content')
<div class="box box-success">
    <div class="box-body">
        <div class="box-body" id="pageForm">
            <h3 class="box-title">{{{ trans("action.{$mode}") }}} {{ trans('pta/pages::common.title') }}</h3>
            <div class="row nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#page">Page</a></li>
                    <li><a href="#seo">SEO</a></li>
                </ul>
                {!! BootForm::openHorizontal(['sm' => [2, 9],'md' => [2, 9]]) !!}

                <div class="tab-content">
                    <div class="tab-pane active" id="page">@include('pta/pages::admin.dashboard.partials.form')</div>
                    <div class="tab-pane" id="seo">@include('pta/meta::admin.form')</div>
                </div>

                <div class="form-group">

                    <label class="col-xs-2 control-label" for="submit">{{{ trans('action.save') }}}</label>

                    <div class="col-xs-9">
                        <button type="submit" class="btn btn-success" id="save"><i class="fa fa-save"></i> {{{ "Save" }}}</button>
                    </div>

                </div>
                {!! BootForm::close() !!}

            </div>
        </div>
    </div>
</div>
@stop


@section('scripts')
<script src="{{url('module/assets/pages/pages.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
<script>

    var pageForm = new Vue({
        el: '#pageForm',
        data: {
            database: @if( old('type', $page->type) === 'database' || old('type', $page->type) === null ) true
            @else false @endif ,
            type_select: null
        },
        ready: function () {
            $(this.$els.layout).selectize({
                create: true,
                sortField: 'text'
            });

            $(this.$els.file).selectize({
                create: true,
                sortField: 'text'
            });

            $(this.$els.active).selectize({
                create: true,
                sortField: 'text'
            });

            $(this.$els.ab).selectize({
                create: true,
                sortField: 'text'
            });
            var select = $(this.$els.typeselect).selectize({
                create: true,
                sortField: 'text'
            });

            this.type_select = select[0].selectize;
            this.type_select.on('change', this.toggleType);

            $('#middleware').selectize();

        },
        methods: {

            toggleType: function (e) {
                var value = this.type_select.getValue();
                if (value === 'database') {

                    this.database = true;
                } else {
                    this.database = false;
                }

            }
        }
    });

    $('#robots').selectize({
        create: true,
        sortField: 'text'
    });

    $('#keywords').selectize({
        delimiter: ',',
        persist: false,
        create: function (input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $(function () {
        $('#content').summernote({
            height: 300,
            tabsize: 2,
            lang: '{{app()->getLocale()}}',
            'fontName':  'Source Sans Pro'
        });

        $('#name').on('keyup', function (e) {

            if (String.prototype.slugify) {
                var input = $(this).data('slugify');

                var slug = $(this).val().slugify();

                $(input).val(slug);
            }
        });
    });

    $('.nav-tabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
  })


</script>


@stop
