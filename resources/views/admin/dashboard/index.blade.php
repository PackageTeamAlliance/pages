@extends ('backend.layouts.master')

@section('page-title')
{{ trans('pta/pages::general.title.index')}}
@stop

@section('main-panel-title')
{{ trans('pta/pages::general.header.index')}}
@stop

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/pages/pages.css')}}">
@stop

@section('content')
<div class="box">
	<div class="box-body">
		<div id="pages-table">
			<v-server-table url="{{{route('pages.dashboard.table')}}}" :columns="columns" :options="options"></v-server-table>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{url('module/assets/pages/pages-index.js')}}"></script>
@stop