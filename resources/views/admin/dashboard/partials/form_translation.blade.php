{{-- Form --}}

{{ csrf_field() }}
<input type="hidden"  name="_token" value="{{ csrf_token() }}">
<input type="hidden"  name="page_locale" value="@{{page_locale}}" id="page_locale" v-model="page_locale">
<input type="hidden"  name="page_id" value="{{$page->id}}" v-model="page_id">
<input type="hidden"  name="translation_id" value="{{{$translation->id ? : ''}}}">

<fieldset style="padding-top:40px;">
	
	<div class="form-group @if($errors->has('locale') ) has-error @endif">

		<label  class="col-xs-2 control-label" for="locale">{{{ trans('pta/pages::model.general.locale') }}}</label>
		<div class="col-xs-9">
			<select class="form-control" name="locale" id="locale"  v-el:locale>
				<option value="">---</option>
				@foreach($locale as $l)
				<option value="{{$l['short_name']}}" @if(old('locale', $translation->locale) === $l['short_name']) selected @endif>{{$l['name']}}</option>
				@endforeach
			</select>
			<span class="help-block">{{{$errors->first('locale')}}}</span>
		</div>
	</div>

	<div class="form-group @if($errors->has('uri') ) has-error @endif ">
		<label class="col-xs-2 control-label" for="uri">{{{ trans('pta/pages::model.general.uri') }}}</label>
		<div class="col-xs-9">
			<div class="input-group">
				<div class="input-group-addon">{{{url('/')}}}/</div>
				<input type="text" class="form-control" name="uri" id="uri" placeholder="{{{ trans('pta/pages::model.general.uri') }}}" value="{{{ old('uri', $translation->uri) }}}">
			</div>

			<span class="help-block">{{{$errors->first('uri')}}}</span>
		</div>
	</div>


	<div class="form-group @if($errors->has('middleware') ) has-error @endif">
		<label class="col-xs-2 control-label" for="middleware">{{{ trans('pta/pages::model.general.middleware') }}}</label>
		<div class="col-xs-9">

			<select name="middleware[]" id="middleware" multiple>
				@foreach(config('middleware') as $p )
				<option value="{{$p['value']}}" @if(in_array($p['value'], $page->middleware ? : [] ) ) selected @endif>{{$p['name']}}</option>
				@endforeach
			</select>
			<span class="help-block">{{{$errors->first('middleware')}}}</span>
		</div>
	</div>

	<div class="form-group @if($errors->has('type') ) has-error @endif">

		<label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.type') }}}</label>

		<div class="col-xs-9">
			<select class="form-control select" name="type" id="type" v-el:typeselect>
				<option @if(old('type', $page->type) === 'database') selected @endif value="database">Database</option>	
				<option @if(old('type', $page->type) === 'file') selected @endif value="file">File</option>	
			</select>
			<span class="help-block">{{{$errors->first('type')}}}</span>	
		</div>
	</div>
	
	<div class="form-group @if($errors->has('content') ) has-error @endif" v-show="database">
		<label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.content') }}}</label>
		<div class="col-xs-9">
			<textarea name="content" id="content" cols="30" rows="10">{{old('content', $translation->content)}}</textarea>
		</div>
	</div>
	

	<div class="form-group @if($errors->has('file') ) has-error @endif" v-show="!database">

		<label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.file') }}}</label>

		<div class="col-xs-9">
			<select class="form-control select" name="file" id="file" v-el:file>
				<option value="">---</option>
				@foreach($pages as $p)
				<option value="{{{ str_replace('.blade.php','', $p->getRelativePathName())}}}" @if( old('file', $page->file) === str_replace('.blade.php','', $p->getRelativePathName())  ) selected @endif >{{{ str_replace('.blade.php','', $p->getRelativePathName())}}}</option>
				@endforeach
			</select>
			<span class="help-block">{{{$errors->first('file')}}}</span>	
		</div>

	</div>
	
	<div v-show="database">
		{!! BootForm::text(trans('pta/pages::model.general.section'), 'section')->value(old('section', $page->section ? : 'content'))->attribute('id', 'section') !!}
	</div>

	<div class="form-group  @if($errors->has('layout') ) has-error @endif">

		<label class="col-xs-2 control-label" for="type">{{{ trans('pta/pages::model.general.layout') }}}</label>

		<div class="col-xs-9">
			<select class="form-control select" name="layout" id="layout" v-el:layout>
				<option value="">---</option>
				@foreach($layouts as $layout)
				<option value="{{{ config('pages.layout_path.name'). str_replace('.blade.php','',$layout->getRelativePathName()) }}}" 
					@if( old('layout', $page->layout) === config('pages.layout_path.name'). str_replace('.blade.php','',$layout->getRelativePathName())  ) 
					selected 
					@endif
					> {{{ config('pages.layout_path.name'). str_replace('.blade.php','',$layout->getRelativePathName()) }}}</option>
					@endforeach
				</select>
				
			</div>

			<span class="help-block">{{{$errors->first('layout')}}}</span>	

		</div>
	</fieldset>
	{{-- End col-md-12 --}}
