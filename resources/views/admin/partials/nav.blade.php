@permission(config('pages.permissions.view.name'))
<li class="{{ Active::pattern('admin/pages*') }} treeview">
	<a href="#">
		<span>{{ trans('pta/pages::menu.main') }}</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu {{ Active::pattern('admin/pages*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/pages*', 'display: block;') }}">
		<li class="{{ Active::pattern('admin/pages') }}">
			<a href="{!! url('admin/pages') !!}"><i class="fa fa-circle-o"></i>{{ trans('pta/pages::menu.dashboard') }}</a>
		</li>
		
		<li class="{{ Active::pattern('admin/pages/create') }}">
			<a href="{!! url('admin/pages/create') !!}"><i class="fa fa-circle-o"></i>{{ trans('pta/pages::menu.create') }}</a>
		</li>

		<li class="{{ Active::pattern('admin/pages/ab/create') }}">
			<a href="{!! route('pages.dashboard.ab.create') !!}"><i class="fa fa-circle-o"></i>{{ trans('pta/pages::menu.create') }} AB</a>
		</li>

		<li class="{{ Active::pattern('admin/pages/ab/create') }}">
			<a href="{!! url('admin/pages/ab') !!}"><i class="fa fa-circle-o"></i>{{ trans('pta/pages::menu.dashboard') }} AB</a>
		</li>
	</ul>
</li>
@endauth