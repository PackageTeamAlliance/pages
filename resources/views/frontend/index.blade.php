@extends($page->get('layout'))

@section('title')
{{{$page['page']->seo->title}}}
@stop

@section('page-meta')

<meta name="description" content="{{$page['page']->seo->description}}"/>
<meta name="keywords" content="{{$page['page']->seo->keywords}}"/>
<meta name="robots" content="{{$page['page']->seo->robots}}"/>
@stop


@section($page->get('section'))

{!! $page->get('content') !!}

@stop
