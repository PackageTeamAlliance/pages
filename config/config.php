<?php

return [

    'name' => 'Pages',
    /*
    * Default prefix to the dashboard.
    */
    'route_prefix' => config('core.admin_uri') . 'pages',
    /*
     * Default path for layouts
     */
    'layout_path' => [
        'frontend' => base_path('resources/views/frontend/layouts'),
        'name' => 'frontend.layouts.',
    ],
    /*
     * Default path for frontend pages
     */
    'page_path' => [
        'frontend' => base_path('resources/views/frontend/pages'),
        'name' => 'frontend.pages',
    ],
    /*
     * Default permission user should have to access the dashboard.
     */
    'security' => [
        'protected' => true,
        'middleware'   => ['web', 'access.routeNeedsPermission:manage-pages'],
        'permission_name'    => 'manage-pages',
    ],
    
    'permissions' => [
        'view' => [
            'name' => 'manage-pages',
            'display_name' => 'Manage Pages'
        ],
        'create' => [
            'name' => 'create-pages',
            'display_name' => 'Create Pages'
        ],
        'edit' => [
            'name' => 'edit-pages',
            'display_name' => 'Edit Pages'
        ],
        'delete' => [
            'name' => 'delete-pages',
            'display_name' => 'Delete Pages'
        ],
        'translation-create' => [
            'name' => 'create-translation-pages',
            'display_name' => 'Create Page Translation'
        ],
        'translation-edit' => [
            'name' => 'edit-translation-pages',
            'display_name' => 'Edit Page Translation'
        ],
        'translation-delete' => [
            'name' => 'delete-translation-pages',
            'display_name' => 'Delete Page Translation'
        ],
    ],

    /*
     * Default url used to redirect user to front/admin of your the system.
     */
    'system_url' => config('core.redirect_url'),
];
