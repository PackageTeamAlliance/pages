<?php

return [
    [ 'name' => 'Web (default)', 'value' => 'web' ],
    [ 'name' => 'Logged in' , 'value' => 'auth'],
    [ 'name' => 'View Admin' , 'value' => 'access.routeNeedsPermission:view-backend'],
];
