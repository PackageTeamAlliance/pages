<?php

namespace Pta\Pages\Traits;

use Illuminate\Database\Eloquent\Collection;

trait RenderTrait
{

    public function returnRender($page)
    {
        $path = '';

        if ($page->type === 'file') {
            $path = config('pages.page_path.name') . "/{$page->file}";

            $content = view($path)->with(compact('page'));
        } else {
            $content = $this->bladeParseString($page->content);
        }

        return new Collection([
            'name' => $page->name,
            'layout' => $page->layout,
            'section' => $page->section,
            'content' => $content,
            'type' => $page->type,
            'path' => $path,
            'ab' => 'false',
            'page' => $page
        ]);
    }

    public function renderTranslation($page)
    {
                $path = '';
        
        if ($translation->type === 'file') {
            $path = config('pages.page_path.frontend') . "/{$translation->file}.blade.php";
            
            $content = view($path)->with(compact('page'));
        } else {
            $content = $this->bladeParseString($page->content);
            // $content = $this->parseBladeCode($translation->content);
        }
        
        return new Collection(['layout' => $translation->layout, 'section' => $translation->section, 'content' => $content, 'type' => $translation->type, 'path' => $path]);
    }


    public function bladeParseString($string, array $args = [])
    {
        $generated = app('blade.compiler')->compileString($string);

        ob_start() and extract($args, EXTR_SKIP);
        
        try {
            eval('?>'.$generated);
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }

        $content = ob_get_clean();

        return $content;
    }


    public function renderAb($page)
    {
        $path = '';

        if ($page->type === 'file') {
            $path = config('pages.page_path.name') . "/{$page->getAbKey('file')}";

            $content = view($path)->with(compact('page'));
        } else {
            $content = $this->bladeParseString($page->getAbKey('content'));
        }

        return new Collection([
            'name' => $page->name,
            'layout' => $page->getAbKey('layout'),
            'section' => $page->getAbKey('section') ? : 'content',
            'content' => $content,
            'type' => $page->type,
            'path' => $path,
            'ab' => true,
            'page' => $page
        ]);
    }
}
