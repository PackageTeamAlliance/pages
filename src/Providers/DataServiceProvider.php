<?php

namespace Pta\Pages\Providers;

use Illuminate\Support\ServiceProvider;
use Pta\Pages\Repositories\PagesRepository;
use Pta\Pages\Repositories\TranslationRepository;

class DataServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
    }
    
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        
        // Register the pages
        $this->app->singleton(PagesRepository::class, function ($app) {
            return new PagesRepository($app);
        });
        
        // Register the translation
         $this->app->singleton(TranslationRepository::class, function ($app) {
            return new TranslationRepository($app);
        });
    }
}
