<?php
namespace Pta\Pages\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use Pta\Pages\Repositories\PagesRepository;
use Pta\Pages\Providers\DataServiceProvider;

class PagesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Providers to register
     *
     * @var array
     */
    protected $providers = [DataServiceProvider::class];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBindings();

        $this->registerViews();

        $this->populateMenus();

        $this->registerTranslations();

        $this->registerMigration();

        $this->registerRoutes();

        $this->registerAssets();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();

        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
    
    public function registerAssets()
    {
        $this->publishes([
          realpath(__DIR__ . '/../../resources/assets/modules') => public_path('module/assets/'),
          ], 'core-assets');
    }
    /**
     * Register Bindings in IoC.
     *
     * @return void
     */
    protected function registerBindings()
    {
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->registerPagesConfig();

        $this->registerMiddlewareConfig();
    }

    public function registerPagesConfig()
    {
        $this->publishes([ realpath(__DIR__ . '/../../config/config.php') => config_path('pages.php'), 'config']);

        $this->mergeConfigFrom(realpath(__DIR__ . '/../../config/config.php'), 'pages');
    }

    protected function registerMiddlewareConfig()
    {
        $this->publishes([ realpath(__DIR__ . '/../../config/middleware.php') => config_path('middleware.php'), 'middleware-config']);

        $this->mergeConfigFrom(realpath(__DIR__ . '/../../config/middleware.php'), 'middleware');
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/pages');

        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/pta/pages'), ]);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/pages');
    }

    public function registerMigration()
    {
        $this->publishes([realpath(__DIR__ . '/../../database/migrations') => database_path('/migrations')],
            'migrations');
    }

    public function populateMenus()
    {
    }

    public function registerRoutes()
    {
        $router = $this->app['router'];

        $prefix = config('pages.route_prefix', 'dashboard');

        $security = config('pages.security.protected', true);


        if (!$this->app->routesAreCached()) {
            $group = [];

            $group['prefix'] = $prefix;

            if ($security) {
                $group['middleware'] = config('pages.security.middleware');
                $group['can'] = config('pages.security.permission_name');
            }

            $router->group($group, function () use ($router) {

                require realpath(__DIR__ . '/../routes.php');
            });

            $group = [];

            $group['namespace'] = \Pta\Pages\Http\Controllers\Frontend::class;

            if (env('CORE_INSTALLED', false)) {
                $this->registerDynamicPageRoutes($router);
            }
        }

        $group['prefix'] = 'v1/track/';

        $router->group($group, function () use ($router) {

            require realpath(__DIR__ . '/../frontend-routes.php');
        });
    }

    public function registerDynamicPageRoutes($router)
    {
        $hasTable = $this->app['cache']->rememberForever("pages.exists", function () {
            return Schema::hasTable('pages');
        });

        if ($hasTable) {
            $this->registerPages($router);
            $this->registerAbPages($router);
        }
    }

    protected function registerPages($router)
    {
        $pages = $this->app[PagesRepository::class]->findActive();
        // dd($pages);
        foreach ($pages as $page) {
            $router->group(['middleware' =>  $page->middleware ], function () use ($router, $page) {
                if ($page->route) {
                    $router->get($page->uri, 'Pta\Pages\Http\Controllers\Frontend\PagesController@index')->name($page->route);
                } else {
                    $router->get($page->uri, 'Pta\Pages\Http\Controllers\Frontend\PagesController@index');
                }

                foreach ($page->translation as $translation) {
                    $router->get($translation->uri, 'Pta\Pages\Http\Controllers\Frontend\PagesController@index');
                }

            });
        }
    }


    protected function registerAbPages($router)
    {
        $pages = $this->app[PagesRepository::class]->findActiveAb();
        // dd($pages);
        foreach ($pages as $page) {
            $router->group(['middleware' =>  $page->middleware ], function () use ($router, $page) {
                if ($page->route) {
                    $router->get($page->uri, 'Pta\Pages\Http\Controllers\Frontend\PagesAbController@index')->name($page->route);
                } else {
                    $router->get($page->uri, 'Pta\Pages\Http\Controllers\Frontend\PagesAbController@index');
                }

                foreach ($page->translation as $translation) {
                    $router->get($translation->uri, 'Pta\Pages\Http\Controllers\Frontend\PagesController@index');
                }

            });
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
