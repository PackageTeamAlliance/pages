<?php

namespace Pta\Pages\Models;

use Pta\Pages\Models\Page;
use Illuminate\Database\Eloquent\Model;

class PagesAbTests extends Model
{
	protected $table = 'pages_ab_tests';

	protected $guarded = ['id'];
	
    public function test()
    {
        return $this->hasOne(Page::class);
    }
}
