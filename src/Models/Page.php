<?php

namespace Pta\Pages\Models;

use Pta\Ab\Models\Ab;
use Pta\Meta\Models\Seo;
use Pta\Ab\Traits\AbTrait;
use Pta\Meta\Traits\SeoTrait;
use Pta\Pages\Models\PagesAbTests;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use SeoTrait, AbTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'pages';

    /**
     * {@inheritDoc}
     */
    protected $guarded = ['id'];
    
    /**
     * {@inheritDoc}
     */
    protected $with = ['translation', 'seo', 'getAb'];

    public function getMiddleWareAttribute($value)
    {
        return json_decode($value);
    }
    
    public function setMiddleWareAttribute($value)
    {
        $this->attributes['middleware'] = json_encode($value);
    }
    
    public function translation()
    {
        return $this->hasMany('Pta\Pages\Models\Translation');
    }
    /**
     * Returns the seo entry that belongs to this entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function seo()
    {
        return $this->morphOne(Seo::class, 'entity');
    }

    public function test()
    {
        return $this->hasOne(PagesAbTests::class);
    }
}
