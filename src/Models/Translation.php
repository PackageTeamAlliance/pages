<?php

namespace Pta\Pages\Models;

use Pta\Meta\Models\Seo;
use Pta\Meta\Traits\SeoTrait;
use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    use SeoTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'page_translations';

    /**
     * {@inheritDoc}
     */
    protected $guarded = ['id'];

    /**
     * {@inheritDoc}
     */
    protected $with = ['seo'];

    public function getMiddleWareAttribute($value)
    {
        return json_decode($value);
    }
    
    public function setMiddleWareAttribute($value)
    {
        $this->attributes['middleware'] = json_encode($value);
    }
    
    public function page()
    {
        return $this->belongsTo('Pta\Pages\Models\Page');
    }

    /**
     * Returns the seo entry that belongs to this entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function seo()
    {
        return $this->morphOne(Seo::class, 'entity');
    }
}
