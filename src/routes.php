<?php

$router->group(['namespace' => 'Pta\Pages\Http\Controllers\Admin'], function () use ($router) {

	/**
	** Pages
	**/
    
    $router->get('/', ['as' => 'pages.dashboard.index', 'uses' => 'PagesController@index']);
    $router->get('/table', ['as' => 'pages.dashboard.table', 'uses' => 'PagesController@table']);

	$router->get('create' , ['as' => 'pages.dashboard.create', 'uses' => 'PagesController@create']);
	$router->post('create', ['as' => 'pages.dashboard.create.process', 'uses' => 'PagesController@store']);
	
	$router->get('{id}/copy' , ['as' => 'pages.dashboard.copy', 'uses' => 'PagesController@copy']);
	$router->post('{id}/copy', ['as' => 'pages.dashboard.copy.process', 'uses' => 'PagesController@store']);

	$router->get('{id}/edit'   , ['as' => 'pages.dashboard.edit'  , 'uses' => 'PagesController@edit']);
	$router->post('{id}/edit'  , ['as' => 'pages.dashboard.edit.process'  , 'uses' => 'PagesController@update']);

	$router->post('{id}/delete/',   ['as' => 'pages.dashboard.delete', 'uses' => 'PagesController@delete']);
	$router->post('update/', ['as'=> 'pages.dashboard.update', 'uses' => 'PagesController@api_update']);


	/**
	** Translation
	**/

	$router->get('{id}/translate'   , ['as' => 'pages.dashboard.translate'  , 'uses' => 'TranslationController@translate']);
	$router->post('{id}/translate'  , ['as' => 'pages.dashboard.translate.process'  , 'uses' => 'TranslationController@translate_store']);

	$router->get('edit/{page_id}/translate/{id}'  , ['as' =>  'pages.dashboard.translate.edit'  , 'uses' => 'TranslationController@translate_edit']);
	$router->post('edit/{page_id}/translate/{id}'  , ['as' => 'pages.dashboard.translate.edit.process'  , 'uses' => 'TranslationController@translate_edit_process']);
	

	/**
	** A/B Pages
	**/
	
	$router->get('ab/', ['as'=> 'pages.dashboard.ab.index', 'uses' => 'PagesABController@index']);
	$router->get('ab/view/{page_id}', ['as'=> 'pages.dashboard.ab.view', 'uses' => 'PagesABController@view']);
	$router->get('ab/table', ['as' => 'pages.dashboard.ab.table', 'uses' => 'PagesABController@table']);
	
	$router->get('ab/create/', ['as'=> 'pages.dashboard.ab.create', 'uses' => 'PagesABController@create']);
	$router->post('ab/create/', ['as'=> 'pages.dashboard.ab.create_process', 'uses' => 'PagesABController@create_process']);
	
	$router->get('ab/edit/{page_id}', ['as'=> 'pages.dashboard.ab.edit', 'uses' => 'PagesABController@edit']);
	$router->post('ab/edit/{page_id}', ['as'=> 'pages.dashboard.ab.edit_process', 'uses' => 'PagesABController@edit_process']);

	$router->post('ab/delete/{page_id}', ['as'=> 'pages.dashboard.ab.delete', 'uses' => 'PagesABController@delete']);

});

