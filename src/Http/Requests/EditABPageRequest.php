<?php

namespace Pta\Pages\Http\Requests;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;

class EditABPageRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow(config('pages.permissions.edit.name'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'layout' => 'required',
            'section' => 'required',
            'file' => 'required_if:type,file',
            'type' => 'required|in:database,file',
            'content' => 'required_if:type,database',
        ];
    }
}
