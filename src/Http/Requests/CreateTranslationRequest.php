<?php

namespace Pta\Pages\Http\Requests;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;

class CreateTranslationRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         return access()->allow(config('pages.permissions.create.name'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'locale' => 'required',
            'page_id' => 'required',
            'uri' => 'required|unique:page_translations',
            'type' => 'required',
            'file' => 'required_if:type,file',
            'layout' => 'required_if:type,database',
            'section' => 'required_if:type,database',
            'content' => 'required_if:type,database',
        ];
    }

}