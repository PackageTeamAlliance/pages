<?php

namespace Pta\Pages\Http\Requests;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;

class EditTranslationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow(config('pages.permissions.translation-edit.name'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locale' => 'required',
            'type' => 'required',
            'file' => 'required_if:type,file',
            'layout' => 'required_if:type,database',
            'section' => 'required_if:type,database',
            'content' => 'required_if:type,database',
            'uri' => 'required|unique:page_translations,uri,' . $this->get('translation_id'),
            'page_locale' => 'required|unique:page_translations,page_locale,' . $this->get('translation_id'),
        ];
    }
}
