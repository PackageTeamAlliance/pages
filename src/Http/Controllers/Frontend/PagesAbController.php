<?php

namespace Pta\Pages\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Pta\Pages\Http\Controllers\Controller;
use Pta\Pages\Repositories\PagesRepository;

class PagesAbController extends Controller
{
    /**
     * The Content repository.
     *
     * @var \Pta\Pages\Repositories\PagesRepository
     */
    protected $pages;


        /**
     * The Content repository.
     *
     * @var \Pta\Pages\Models\Page
     */
    protected $page;

    //ab variable set on users cookie
    protected $ab;

    //uri key for cookie based on current uri
    protected $uri;

    /**
     * Constructor.
     *
     * @param  \Pta\Content\Repositories\ContentRepositoryInterface $contents
     * @return void
     */

    public function __construct(Container $app, PagesRepository $pages)
    {
        $this->pages = $pages;

        $this->repository = $pages;
    }

    public function index(Request $request)
    {
        $this->uri = static::$router->current()->getUri();
        
        $p = $this->pages->findByURI($this->uri);

        $this->ab = $this->handle($request, $p);

        if ($this->ab) {
            $this->page = $this->pages->renderAb($p);
        } else {
            $this->page = $this->pages->returnRender($p);
        }
        
        if ($request->cookie($this->uri)) {
            return $this->returnResponse();
        }

        return $this->returnResponseWithCookie();
    }


    public function handle(Request $request, $p)
    {
        $page = $this->pages->findByURI($this->uri);
        
        $p = $page->fresh();
        
        if (is_null($request->cookie($this->uri))) {
            $a = $p->getAb->a_view;
            $b = $p->getAb->b_view;

            if ($a < $b) {
                return false;
            } elseif ($b < $a) {
                return true;
            } else {
                return mt_rand(0, 1);
            }
        } else {
            return $request->cookie($this->uri);
        }
    }

    protected function returnResponseWithCookie()
    {
        $page = $this->page;
        $cookie = cookie($this->uri, $this->ab, (60 * 24 * 365));
        return response()->view('pta/pages::frontend.index', compact('page'))
            ->withCookie($cookie);
    }

    protected function returnResponse()
    {
        $page = $this->page;
        return view('pta/pages::frontend.index', compact('page'));
    }

    public function track(Request $request)
    {
        $page = $this->pages->find($request->get('page_id'));
        
        $this->pages->increment_visits($page, $request->get('ab'));
    }

    public function track_cta(Request $request)
    {
        $page = $this->pages->find($request->get('page_id'));
            
        $this->pages->increment_click($page, $request->get('ab'));
    }

}
