<?php

namespace Pta\Pages\Http\Controllers\Frontend;

use Illuminate\Container\Container;
use Pta\Pages\Http\Controllers\Controller;
use Pta\Pages\Repositories\PagesRepository;
use Pta\Pages\Repositories\TranslationRepository;

class PagesController extends Controller
{
    /**
     * The Content repository.
     *
     * @var \Pta\Pages\Repositories\PagesRepository
     */
    protected $pages;

    /**
     * The Content repository.
     *
     * @var \Pta\Content\Repositories\TranslationRepository
     */
    protected $translation;

    protected $repository;

    /**
     * Constructor.
     *
     * @param  \Pta\Content\Repositories\ContentRepositoryInterface $contents
     * @return void
     */
    public function __construct(Container $app, PagesRepository $pages, TranslationRepository $translation)
    {
        $this->pages = $pages;

        $this->translation = $translation;

        $this->locale = $app->getLocale();

        if ($this->locale === config('languages.default.short_name', 'en')) {
            $this->setRepository($this->pages);
        } else {
            $this->setRepository($this->translation);
        }
    }

    public function index()
    {
        $uri = static::$router->current()->getUri();

        if ($page = $this->getRepository()->render($uri)) {
            return $this->getView('frontend.index', compact('page'));
        }
        
        if (!$page) {
            $redirect = $this->translation->createModel()->whereHas('page', function ($query) use ($uri) {
                // $query->where('uri', $uri);
                $query->where('locale', app()->getLocale());
            })->get();

            if (! $redirect->count()) {
                $redirect = $this->pages->createModel()->whereHas('translation', function ($query) use ($uri) {
                    $query->where('uri', $uri);
                })->first();
            }


            if ($redirect) {
                return redirect($redirect->first()->uri)->withFlashSuccess(trans('alerts.backend.users.created'));
            }
        }

        return abort(404);
    }


    protected function switchRepository()
    {
        if ($this->locale === config('languages.default.short_name', 'en')) {
            $this->setRepository($this->translation);
        } else {
            $this->setRepository($this->pages);
        }
    }

    protected function setRepository($repository)
    {
        $this->repository = $repository;
    }

    protected function getRepository()
    {
        return $this->repository;
    }
}
