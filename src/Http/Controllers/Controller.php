<?php

namespace Pta\Pages\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function getView($view, $data = [])
    {

        return view("pta/pages::{$view}")->with($data);
    }
}
