<?php

namespace Pta\Pages\Http\Controllers\Admin;

use Pta\Meta\Models\Seo;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Pta\Pages\Http\Controllers\Controller;
use Pta\Pages\Repositories\PagesRepository;
use Pta\Pages\Repositories\TranslationRepository;
use Pta\Pages\Http\Requests\EditTranslationRequest;
use Pta\Pages\Http\Requests\CreateTranslationRequest;

class TranslationController extends Controller
{
    private $translation;
    
    private $pages;

    private $manager;

    protected $fields = ['id','name','type', 'active', 'uri', 'ab'];

    public function __construct(TranslationRepository $translation, PagesRepository $pages, Filesystem $manager)
    {
        $this->translation = $translation;

        $this->pages = $pages;

        $this->manager = $manager;
    }
  
    public function translate($id)
    {
        return $this->showTranslateForm('translate', $id, null);
    }

    public function translate_store($id, CreateTranslationRequest $request)
    {
        $this->translation->store(null, $request->all());

        return redirect()->route('pages.dashboard.index')->withFlashSuccess(trans("pta/pages::message.success.create"));
    }

    public function translate_edit($page_id, $id)
    {
        return $this->showTranslateForm('edit', $page_id, $id);
    }

    public function translate_edit_process($page_id, $id, EditTranslationRequest $request)
    {
        $this->translation->store($id, $request->all());

        return redirect()->back()->withFlashSuccess(trans("pta/pages::message.success.update"));
    }

    protected function showTranslateForm($mode, $page_id, $id = null)
    {
        if (isset($id)) {
            if (!$translation = $this->translation->find($id)) {
                flash()->error(trans('pta/pages::message.not_found', compact('id')));

                return redirect()->route('pages.dashboard.index');
            }
        } else {
            $translation = $this->translation->retrieveModel();
        }

 
        $collection = collect(config('languages.locale'));

        $locale = $collection->filter(function ($value, $key) {
            return $value['short_name'] != config('languages.default.short_name');

        });

        $page = $this->pages->find($page_id);

        $layouts = $this->manager->allFiles(config('pages.layout_path.frontend'));

        $pages = $this->manager->allFiles(config('pages.page_path.frontend'));

        if ($translation->hasSeo()) {
            $seo = $translation->seo;
        } else {
            $seo = Seo::getModel();
        }

        return $this->getView('admin.dashboard.translate_form',
                compact('mode', 'page', 'pages', 'layouts', 'locale', 'translation', 'seo'));
    }
}
