<?php

namespace Pta\Pages\Http\Controllers\Admin;

use Pta\Meta\Models\Seo;
use Illuminate\Http\Request;
use Pta\Pages\Models\PagesAbTests;
use Illuminate\Filesystem\Filesystem;
use Pta\Pages\Http\Controllers\Controller;
use Pta\Pages\Repositories\PagesRepository;
use Pta\Pages\Http\Requests\EditABPageRequest;
use Pta\Pages\Http\Requests\PageManageRequest;
use Pta\Pages\Http\Requests\CreateABPageRequest;
use Pta\Pages\Http\Requests\DeleteABPageRequest;
use Pta\Pages\Repositories\TranslationRepository;

class PagesABController extends Controller
{
    protected $fields = ['id','name','type', 'active', 'uri', 'ab'];

    public function __construct(PagesRepository $pages, TranslationRepository $translation, Filesystem $manager)
    {
        $this->pages = $pages;

        $this->translation = $translation;

        $this->manager = $manager;
    }

    public function index(PageManageRequest $request)
    {
        return $this->getView('admin.dashboard.ab.index');
    }

    public function table(PageManageRequest $request)
    {
        return $this->pages->get($request->all(), $this->fields, ['ab' => 1]);

        $pages = collect($p['data']);

        $p['data'] = $pages->where('ab', 1);
       
        $p['count'] = count($p['data']);
    
        return $p;
    }

    public function create(PageManageRequest $request)
    {
        $pages = $this->pages->findAll();

        return $this->getView('admin.dashboard.ab.create', compact('pages'));
    }

    public function create_process(CreateABPageRequest $request)
    {
        $this->pages->createAbPage($request->except('_token'));

        return $this->getView('admin.dashboard.ab.index');
    }

    public function edit(PageManageRequest $request, $page_id)
    {
        return $this->showForm('update', $page_id);
    }

    public function edit_process(EditABPageRequest $request)
    {
        $type = $this->pages->abUpdate($request->all()) ? 'success' : 'error';

        return redirect()->route('pages.dashboard.ab.index')->withFlashSuccess(trans("pta/pages::message.success.update"));
    }

    public function view($id)
    {
        $page = $this->pages->findFresh($id);

        return $this->getView('admin.dashboard.ab.view', compact('page'));
    }

    public function delete(DeleteABPageRequest $request, $page_id)
    {
        $type = $this->pages->deleteAb($page_id, ['ab' => 0]) ? 'success' : 'error';

        if ($request->ajax() && $type === 'success') {
            return http_response_code(200);
        }
        
        return redirect()->route('pages.dashboard.ab.index')->withFlashSuccess("AB Page updated");
    }

    protected function showForm($mode, $id = null)
    {
        if (isset($id)) {
            if (!$page = $this->pages->find($id)) {
                return redirect()->route('pages.dashboard.ab.index')->withFlashDanger(trans('pta/pages::message.not_found', compact('id')));
            }
        } else {
            $page = $this->pages->createModel();
        }

        $layouts = $this->manager->allFiles(config('pages.layout_path.frontend'));

        $pages = $this->manager->allFiles(config('pages.page_path.frontend'));

        return $this->getView('admin.dashboard.ab.form', compact('mode', 'page', 'layouts', 'pages'));
    }
    
    public function track(Request $request)
    {
        //benchmark 
    }
}
