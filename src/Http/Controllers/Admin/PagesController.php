<?php

namespace Pta\Pages\Http\Controllers\Admin;

use Pta\Meta\Models\Seo;
use Pta\Pages\Models\Page;
use Illuminate\Http\Request;
use Pta\Pages\Entities\Translation;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Input;
use Pta\Pages\Http\Controllers\Controller;
use Pta\Pages\Repositories\PagesRepository;
use Pta\Pages\Http\Requests\EditPageRequest;
use Pta\Pages\Http\Requests\CreatePageRequest;
use Pta\Pages\Http\Requests\DeletePageRequest;
use Pta\Pages\Http\Requests\EditTranslationRequest;
use Pta\Pages\Http\Requests\CreateTranslationRequest;
use Pta\Pages\Http\Controllers\Admin\BasePagesController;
use Pta\Pages\Repositories\TranslationRepositoryInterface;

class PagesController extends Controller
{

    /**
     * The Content repository.
     *
     * @var \Pta\Pages\Repositories\PagesRepository
     */
    protected $pages;

        /**
     * The Filesystem Manager.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $manager;

    protected $fields = ['id','name','type', 'active', 'uri', 'ab'];
    
    /**
     * Constructor.
     *
     * @param  \Pta\Content\Repositories\ContentRepositoryInterface $contents
     * @return void
     */
    public function __construct(PagesRepository $pages, Filesystem $manager)
    {
        $this->pages = $pages;

        $this->manager = $manager;
    }

    public function index()
    {
        return $this->getView('admin.dashboard.index');
    }

    public function table(Request $request)
    {
        return $this->pages->get($request->all(), $this->fields);
    }

    public function create()
    {
        return $this->showForm('create');
    }

    public function copy($id)
    {
        return $this->showForm('copy', $id);
    }

    public function store(CreatePageRequest $request, $id = null)
    {
        $this->pages->store(null, $this->sanitize($request));

        return redirect()->route('pages.dashboard.index')->withFlashSuccess(trans("pta/pages::message.success.create"));
    }

    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    public function update($id, EditPageRequest $request)
    {
        $page = $this->pages->store($id, $this->sanitize($request));
        
        return redirect()->route('pages.dashboard.index')->withFlashSuccess(trans("pta/pages::message.success.update"));
    }

    public function api_update(Request $request)
    {
        $this->pages->toggleStatus($request->get('pk'));
        
        return http_response_code(200);
    }


    public function delete(DeletePageRequest $request, $id)
    {
        $type = $this->pages->delete($id) ? 'success' : 'error';

        
        if ($request->ajax() && $type === 'success') {
            return http_response_code(200);
        }
        
        return redirect()->route('pages.dashboard.index')->withFlashSuccess(trans("pta/pages::message.{$type}.delete"));
    }

    protected function sanitize($request)
    {
        return array_except($request->all(), 'files');
    }

    
    protected function showForm($mode, $id = null)
    {
        if (isset($id)) {
            if (!$page = $this->pages->find($id)) {
                flash()->error(trans('pta/pages::message.not_found', compact('id')));

                return redirect()->route('pages.dashboard.index');
            }
        } else {
            $page = $this->pages->createModel();
        }


        $layouts = $this->manager->allFiles(config('pages.layout_path.frontend'));

        $pages = $this->manager->allFiles(config('pages.page_path.frontend'));

        if ($page->hasSeo()) {
            $seo = $page->seo;
        } else {
            $seo = Seo::getModel();
        }

        return $this->getView('admin.dashboard.form', compact('mode', 'page', 'layouts', 'pages', 'seo'));
    }
}
