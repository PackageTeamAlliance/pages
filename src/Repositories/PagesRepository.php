<?php

namespace Pta\Pages\Repositories;

use Carbon\Carbon;
use Pta\Pages\Traits\RenderTrait;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;
use Pta\Support\Traits\VueTableTrait;
use Rinvex\Repository\Repositories\EloquentRepository;

class PagesRepository extends EloquentRepository
{
    use VueTableTrait, RenderTrait;


    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $container
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->setContainer($container)
            ->setModel(\Pta\Pages\Models\Page::class)
            ->setRepositoryId(md5('pta.pages.cache'));
    }

    public function findPage($id)
    {
        return $this->find($id);
    }

    public function findActive()
    {
        return $this->findWhere(['active', '=', 1])
            ->filter(function ($page, $key) {
                return $page->ab == false;
            });
    }

    public function findActiveAb()
    {
        return $this->findWhere(['active', '=', 1])
            ->filter(function ($page, $key) {
                return $page->ab == true;
            });
    }

    public function findFresh($id)
    {
        return $this->createModel()->find($id);
    }
  
    /**
     * {@inheritDoc}
     */
    public function findByURI($uri)
    {
        return $this->findWhere(['uri', '=', $uri])->first();
    }

    public function findABPages()
    {
        return $this->findWhere(['ab', '=', 1]);
    }

    public function render($uri, $locale = 'en')
    {
        $page = $this->findWhere(['uri', '=', $uri])
            ->filter(function ($page, $key) {
                return $page->active == true;
            })->first();

        if ($page) {
            return $this->returnRender($page);
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        $data = array_except($input, ['page_id', 'title', 'description', 'keywords', 'robots', 'image']);

        $return = !$id ? $this->create($data) : $this->update($id, $data);

        list($status, $instance) = $return;

        $instance->storeSeo($input);
        
        return $instance;
    }

    public function createAbPage($data)
    {
        $entry = $this->update($data['page_id'], ['ab' => 1]);

        list($status, $page) = $entry;

        $page->createAb(['name' => $data['name'] ]);
    }

    public function deleteAb($id, $data)
    {
        $page = $this->find($id);
        
        $entry = $this->update($page->id, ['ab' => 0]);

        list($status, $insance) = $entry;

        $insance->test()->delete();

        $insance->getAb()->delete();
    }
    
    public function abUpdate($data)
    {
        $page = $this->find($data['page_id']);

        $ab_data = array_except($data, ['_token', 'page_id', 'name']);

        $page->getAb()->update(['content' => json_encode($ab_data)]);

        return 'success';
    }

    public function increment_click($page, $ab)
    {
        if ($ab === 'false' || $ab == false) {
            $page->getAb()->increment('a_clicks');
        } else {
            $page->getAb()->increment('b_clicks');
        }

        $this->update($page->id, ['updated_at' => Carbon::now()]);
    }

    public function increment_visits($page, $ab)
    {
        $page->getAb()->increment('requests', 1);
        
        if ($ab === 'false' || $ab == false) {
            $page->getAb()->increment('a_view', 1);
        } else {
            $page->getAb()->increment('b_view', 1);
        }

        $this->update($page->id, ['updated_at' => Carbon::now()]);
    }
}
