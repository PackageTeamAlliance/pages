<?php

namespace Pta\Pages\Repositories;

use Illuminate\Container\Container;
use Pta\Pages\Traits\RenderTrait;
use Pta\Support\Traits\VueTableTrait;
use Rinvex\Repository\Repositories\EloquentRepository;

class TranslationRepository extends EloquentRepository
{
    use VueTableTrait, RenderTrait;
    
    /**
     * The Data handler.
     *
     * @var \Pta\Content\Handlers\DataHandlerInterface
     */
    protected $data;
    
    /**
     * The Eloquent content model.
     *
     * @var string
     */
    protected $model;
    
    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $container
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->setContainer($container)
            ->setModel(\Pta\Pages\Models\Translation::class)
            ->setRepositoryId(md5('pta.pages.translation.cache'));
    }

    
    public function render($uri)
    {
        if ($page = $this->where('uri', '=' , $uri)->where('locale' ,'=', app()->getLocale()) ->first()) {
            return $this->renderTranslation($page);
        }
   
        return false;
    }

    
    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        $data = array_except($input, ['translation_id','files', 'title', 'description', 'keywords', 'robots']);
        
        $return = !$id ? $this->create($data) : $this->update($id, $data);

        list($status, $instance) = $return;

        $instance->storeSeo($data);
        
        return $instance;
    }
}
