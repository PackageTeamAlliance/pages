<?php

$router->group([], function () use ($router) {
    
    $router->post('view', ['as' => 'ab.pages.track.track', 'uses' => 'PagesAbController@track']);
    $router->post('cta', ['as' => 'ab.pages.track.track_cta', 'uses' => 'PagesAbController@track_cta']);


});

