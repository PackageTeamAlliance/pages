<?php

namespace Pta\Pages\Console\Installers;

use Illuminate\Console\Command;
use Pta\Pulse\Console\Contracts\Installer;

class PagesInstaller implements Installer
{

    /**
     * @var Command
     */
    protected $command;

    public function run(Command $command)
    {

        $this->command = $command;

        $this->command->call('module:migrate', ['module' => 'pages']);

        $this->command->call('module:seed', ['module' => 'pages']);
    }
}
